﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NJHouseThings.Models.treeView
{
    public class TreeViewRepository : ITreeViewRepository
    {
        //public TreeView treeView;
        private bool firstLoad;
        private object vSender;
        private EventArgs vEvent;
        private TreeView vTreeView;
        private TreeViewEventArgs vTreeViewEvent;
        private Button vActionTreeView;
        private TextBox vTreeViewTxt;
        private ImageList imgList;

        public TreeViewRepository() {
            firstLoad = false;

            imgList = new ImageList();
            //icoFolder.Images.Add(Image.FromFile(new Bitmap(Properties.Resources.ico_folder));
            imgList.Images.Add(Properties.Resources.ico_folder);
        }

        public void setProperty(object pSender, EventArgs pEvent, TreeView pTreeView, Button pActionTreeView, TextBox pTreeViewTxt)
        {
            //vSender = pSender;
            //vEvent = pEvent;
            vTreeView = pTreeView;
            vActionTreeView = pActionTreeView;
            vTreeViewTxt = pTreeViewTxt;

            // 폴더 확장 이벤트 등록
            vTreeView.AfterExpand+=vTreeView_AfterExpand;
            // 폴더 선택시 이벤트 등록
            vTreeView.AfterSelect += vTreeView_AfterSelect;
            // 트리보기 번튼 클릭 이벤트 등록
            vActionTreeView.Click += vActionTreeView_Click;

            vTreeView.ImageList = imgList;
        }

        /// <summary>
        /// 탭 이름설정
        /// </summary>
        /// <returns></returns>
        public string setTabPageTitle()
        {            
            return "TreeView";
        }

        public void LoadDirectory()
        {
            try
            {
                firstLoad = true;

                string[] drivers = Directory.GetLogicalDrives();
                foreach (string drive in drivers)
                {
                    TreeNode root = new TreeNode(drive);
                    vTreeView.Nodes.Add(root);
                    vTreeView.ImageIndex = 0;                    
                    

                    DirectoryInfo dir = new DirectoryInfo(drive);
                    if (dir.Exists)
                    {
                        AddDirectoryNodes(root, dir, false);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        /// <summary>
        /// 하위 디렉토리 검색을 위한
        /// 재귀 함수
        /// </summary>
        /// <param name="root"></param>
        /// <param name="dir"></param>
        /// <param name="isLoop"></param>
        private void AddDirectoryNodes(TreeNode root, DirectoryInfo dir, bool isLoop)
        {
            try
            {
                DirectoryInfo[] drivers = dir.GetDirectories();
                foreach (DirectoryInfo drive in drivers)
                {
                    // 디렉토리명을 파라미터로 전달받은 상위 드라이버에 넣는다.
                    TreeNode childRoot = new TreeNode(drive.Name);
                    root.Nodes.Add(childRoot);

                    // 처음 드라이버 검색을 제외한 디렉토리 검색일때 하위디렉토리 여부 검색
                    if (isLoop)
                    {
                        AddDirectoryNodes(childRoot, drive, false);
                    }
                }
            }
            catch (Exception ex)
            {
                if (!firstLoad)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        /// <summary>
        /// 폴더 확장시 발생이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void vTreeView_AfterExpand(object sender, TreeViewEventArgs e)
        {
            DirectoryInfo dir = new DirectoryInfo(e.Node.FullPath);
            e.Node.Nodes.Clear();
            AddDirectoryNodes(e.Node, dir, true);
        }

        /// <summary>
        /// 폴더 선택시 발생 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void vTreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            vSender = sender; vTreeViewEvent = e;
        }

        /// <summary>
        /// 트리보기 버튼 클릭시...
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void vActionTreeView_Click(object sender, EventArgs e)
        {
            if (vSender == null)
            {
                MessageBox.Show("폴더를 선택해주세요.");
                return;
            }
            string fullPath = vTreeViewEvent.Node.FullPath;
            string fullPathText = "";
            int dNum = 0; // 뎁스

            DirectoryInfo dirInfo = new DirectoryInfo(fullPath);

            bool fileExists = false;
            string[] fileInfo = Directory.GetFiles(fullPath, "*", SearchOption.TopDirectoryOnly);
            if (fileInfo.Count() > 0) fileExists = true;

            if (fileExists)
            {
                string fileLine = "    ";
                int ingCnt2 = 1;    // 몇번째 파일인지...
                FileInfo[] files = dirInfo.GetFiles();

                if (dirInfo.Exists) fileLine = "│    ";
                foreach (FileInfo file in files)
                {
                    fullPathText += fileLine + file.Name + Environment.NewLine;
                    ingCnt2++;
                }
            }

            if (dirInfo.Exists)
            {
                DirectoryInfo[] dirs = dirInfo.GetDirectories();
                TreeNode root = new TreeNode(fullPath);

                int ingCnt = 1; // 몇번째 폴더인지...
                string folderLine = "├─ ";

                foreach (DirectoryInfo dir in dirs)
                {
                    if (dirs.Count() == ingCnt) folderLine = "└─";
                    fullPathText += folderLine + dir.Name + Environment.NewLine;

                    // Root 부터의 Depth 내용을 알기위함
                    Dictionary<int, bool> depthHis = new Dictionary<int, bool>();
                    depthHis.Add(dNum, (dirs.Count() == ingCnt));

                    // 하위폴더 및 파일
                    fullPathText += AddDirectoryNodesStr(root, dir, true, dNum, dirs.Count() == ingCnt, depthHis);

                    ingCnt++;
                }
            }

            vTreeViewTxt.Text = fullPathText;
        }

        private string AddDirectoryNodesStr(TreeNode root, DirectoryInfo dir, bool isLoop, int dNum, bool dChk, Dictionary<int, bool> depthHis)
        {
            string rtnDir = ""; // 결과값 반환
            string folderLine = "├─";
            int ingCnt = 0; // 몇번째 폴더인지...
            bool fileLineTF = false;

            try
            {
                dNum++;

                DirectoryInfo[] _dirs = dir.GetDirectories();

                // 파일
                FileInfo[] _files = dir.GetFiles();
                if (_dirs.Count() > 0) fileLineTF = true;
                foreach (FileInfo _file in _files)
                {
                    rtnDir += AddSpaceFile(dNum, dChk, depthHis, fileLineTF) + _file.Name + Environment.NewLine;
                }

                // 폴더

                foreach (DirectoryInfo _dir in _dirs)
                {
                    ingCnt++;

                    // 현재경로 설정
                    TreeNode chRoot = new TreeNode(_dir.Name);

                    if (_dirs.Count() == ingCnt) folderLine = "└─";

                    int deptDelCnt = depthHis.Count();
                    for (int i = 0; i <= deptDelCnt-1; i++)
                    {
                        if (i >= dNum)
                        {
                            depthHis.Remove(i);
                        }
                    }

                    depthHis.Add(dNum, _dirs.Count() == ingCnt);

                    rtnDir += AddVline(depthHis) + folderLine + _dir.Name + Environment.NewLine;

                    if (isLoop)
                        rtnDir += AddDirectoryNodesStr(chRoot, _dir, true, dNum, _dirs.Count() == ingCnt, depthHis);
                }
            }
            catch (Exception ex)
            {
                rtnDir += ex.Message.ToString();
            }

            return rtnDir;
        }

        private string AddSpaceFile(int dNum, bool dChk, Dictionary<int, bool> dent, bool afterFolderTF)
        {
            string spaceStr1 = "│  ";
            string spaceStr2 = "    ";
            string space = "";

            for (int i = 0; i < dent.Count(); i++)
            {
                if (!dent[i])
                {
                    space += spaceStr1;
                }
                else
                {
                    space += spaceStr2;
                }
            }

            if (afterFolderTF)
            {
                space += spaceStr1;
            }
            else
            {
                space += spaceStr2;
            }

            return space;
        }

        private string AddVline(Dictionary<int, bool> dept)
        {
            string rtnStr = "";
            string spaceStr1 = "│  ";
            string spaceStr2 = "    ";


            for (int i = 0; i < dept.Count(); i++)
            {
                if (i != dept.Count() - 1)
                {
                    if (!dept[i])
                    {
                        rtnStr += spaceStr1;
                    }
                    else
                    {
                        rtnStr += spaceStr2;
                    }

                }
            }

            return rtnStr;
        }
    }
}
