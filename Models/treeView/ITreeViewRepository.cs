﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NJHouseThings.Models.treeView
{
    public interface ITreeViewRepository
    {
        string setTabPageTitle();
        void setProperty(object pSender, EventArgs pEvent, TreeView pTreeView, Button pActionTreeView, TextBox pTreeViewTxt);
        void LoadDirectory();
    }
}
