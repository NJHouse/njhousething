﻿using NJHouseThings.Models.treeView;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class things : Form
    {
        public static ITreeViewRepository treeViewRepository = new TreeViewRepository();
        private bool[] arrTabs = new bool[10];

        ArrayList tabs = new ArrayList();

        public things() {
            InitializeComponent();

            arrTabs[0] = false;
        }

        private void things_Load(object sender, EventArgs e)
        {
            mtTabSet();
            if (!arrTabs[0]) {
                mtTreeView(sender, e);  // -- -- 트리뷰 처리
            }

            //트레이 아이콘과 컨텍스트메뉴 연결
            notifyIcon1.ContextMenuStrip = contextMenuStrip1;
        }

        private void mtTabSet()
        {
            tabPage1.Text = treeViewRepository.setTabPageTitle();
            tabPage2.Text = "인코더";
        }


        // 트리메뉴 처리
        private void mtTreeView(object sender, EventArgs e)
        {
            arrTabs[0] = true;
            treeViewRepository.setProperty(sender, e, treeView1, actionTree, treeViewTxt);
            treeViewRepository.LoadDirectory();
        }

        protected override bool ProcessDialogKey(Keys keyData)
        {
            switch (keyData)
            {
                case Keys.A | Keys.Control:
                    if (this.ActiveControl is TextBox)
                    {
                        TextBox txt = (TextBox)this.ActiveControl;
                        txt.SelectionStart = 0;
                        txt.SelectionLength = txt.Text.Length;
                        return true;
                    }
                    break;
            }
            return base.ProcessDialogKey(keyData);
        }


        #region 트레이 아이콘
        private void things_Resize_1(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == WindowState)
            {
                Hide();
            }
        }

        private void notifyIcon1_DoubleClick_1(object sender, EventArgs e)
        {
            Show();
            WindowState = FormWindowState.Normal;
        }
        #endregion        

        private void things_FormClosing_1(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;    // 이벤트 취소

            this.Visible = false;
            // this.Hide();
            // 둘중 한개 쓰면 됨. 이둘을 사용하여 최소화 하면 alt+tab 실행 시 출력이 되지 않음
            // alt+tab 사용시 나타나게 하는 방법
            // this.WindowState = FormWindowState.Minimized;    // 최소화
            // this.ShowinTaskbar = false;  // 화면 하단에 나타나는것 해제
        }

        private void showToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Show();
            WindowState = FormWindowState.Normal;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.ExitThread();
            Environment.Exit(0);
        }
    }
}
