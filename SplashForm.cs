﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class SplashForm : Form
    {
        delegate void TestDelegate(string msg);
        delegate void TestDelegate2();

        public SplashForm()
        {
            InitializeComponent();
        }

        private void SplashForm_Load(object sender, EventArgs e)
        {
            Thread thread = new Thread(Thread1);
            thread.Start();
        }

        private void showText(string msg)
        {
            lblCount.Text = msg;
        }

        private void formClose()
        {
            this.Close();
        }
        
        private void Thread1()
        {
            for (int i = 10; i > 0; i--)
            {
                this.Invoke(new TestDelegate(showText), i.ToString());
                Thread.Sleep(100);
            }

            this.Invoke(new TestDelegate2(formClose));
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }
    }
}
